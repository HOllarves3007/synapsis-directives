'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('Synapsis', [
    'Synapsis.directives',
    'ngRoute'
]);

app.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');
  $routeProvider
    .when("/", {
        templateUrl:'components/Main/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
    })
    .when('/videoChat', {
        templateUrl:'components/Video_Chat/videoChat.html',
        controller: 'VideoChatCtrl',
        controllerAs: 'videoChat'
    })
    .when('/chat', {
        templateUrl:'components/Chat/chat.html',
        controller:'ChatCtrl',
        controllerAs:'chat'
    })
    .otherwise(
        {redirectTo: '/'}
    );
}]);

app.controller('MainCtrl', ['$scope', MainCtrl]);
app.controller('VideoChatCtrl', ['$scope', VideoChatCtrl]);
app.controller('ChatCtrl', ['$scope', ChatCtrl]);

function MainCtrl($scope){
    //assign control to visual model
    var vm = this;
    //Doctor dummy array
    vm.doctors =  [
        {
            id: 1,
            name:'Dr. Einstein'
        },
        {
            id: 2,
            name: 'Dr. Xavier'
        },
        {
            id: 3,
            name: 'Dr. Evil'
        }
    ];
}

function VideoChatCtrl($scope){
    //assing control to visual model
    var vm = this;
    //Short description
    vm.desc = 'Simple Web-RTC directives';
    //Dummy data
    vm.myUser = {
        id: '1',
        name: 'Henry'
    };
    vm.myDoctor = {
        id: '2',
        name: 'Dr. Henry'
    };
    //Switches and directive parameters
    $scope.isBroadcasting = false;
    $scope.minWidth = 640;
    $scope.maxNumPeers = 2;
    $scope.startBroadcasting = function(){
        $scope.isBroadcasting = true;
    };
}
function ChatCtrl($scope){

    //assing control to visual model
    var vm = this;
    //Dummy data
    vm.myUser = {
        id: '1',
        name: 'Henry'
    };
    vm.myDoctor = {
        id: '2',
        name: 'Dr. Henry'
    };

    $scope.maxNumPeers = 2;
    $scope.message = '';

    // print broadcasted messages?
    $scope.sendMessage = function sendMessage() {
        $scope.$broadcast('messageAll', $scope.message);
    };
}
