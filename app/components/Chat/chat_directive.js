var module = angular.module('Synapsis.directives');

module.directive('chat', function(){
    return {
        restrict: 'E',
        templateUrl: 'components/Chat/chatDirective.html',
        transclude: true,
        scope: {
            name:'@',
            userid: '@',
            doctorid: '@',
            maxNumPeers: '='
        },
        controller: function ($scope, $rootScope) {
            console.log('chat directive = ', $scope);
            //Setting global variables
            var userId = $scope.userid;
            var doctorId = $scope.doctorid;
            var webrtc;

            formRTCOptions = function () {
                //Creating options object
                var webRTCOptions = {
                    //debugging capabilities
                    debug: false,
                    //media properties
                    media: {
                        audio: false,
                        video: false
                    }
                };

                return webRTCOptions;
            };
            //Creating options object
            var webRTCOptions = formRTCOptions();
            //Instantiating webrtc object
            webrtc = new SimpleWebRTC(webRTCOptions);
            console.log('object formed', webrtc);
            //Passing webrtc object instance to $rootScope
            $rootScope.webrtc = webrtc;
            //When ready to connect
            webrtc.on('readyToCall', function () {
                //Creating room name
                $scope.room = userId + '-' + doctorId;
                console.log('joining room = ', $scope.room);
                //Joining room
                webrtc.joinRoom($scope.room);
                //Initializing event handlers
                rtcEventHandler(webrtc);
            });
            //Leave room handler
            $scope.$on('leaveRoom', function leaveRoom() {
                console.log('leaving $scope.room', $scope.room);
                if (!$scope.room) {
                    return;
                }
                webrtc.leaveRoom($scope.room);
            });
            //WebRTC Event handlers
            function rtcEventHandler(webrtc){
                webrtc.on('videoRemoved', function (video, peer) {
                    console.log('videoRemoved!');
                });
                webrtc.on('videoAdded', function (video, peer) {
                    console.log('video added from peer nickname', peer.nick);
                });
                webrtc.on('iceFailed', function (peer) {
                    console.error('ice failed', peer);
                });
                webrtc.on('connectivityError', function (peer) {
                    console.error('connectivity error', peer);
                });
                webrtc.on('joinedRoom', function (name) {
                    console.log('joined room "%s"', name);
                    var peers = webrtc.getPeers();
                    if (peers && Array.isArray(peers) &&
                        peers.length > $scope.maxNumPeers) {
                        console.error('Too many people in the room, leaving');
                        webrtc.leaveRoom();
                        return;
                    }
                });
                //Chat messaging handler
                $scope.$on('messageAll', function (event, message) {
                    if (message && webrtc) {
                        webrtc.sendDirectlyToAll(JSON.stringify(message));
                    }
                });
            }
        }
    }
});