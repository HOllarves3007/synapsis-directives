var module = angular.module('Synapsis.directives');

module.directive('videoChat', function(){
    return {
        restrict: 'E',
        transclude: true,
        templateUrl:'components/Video_Chat/videoDirective.html',
        scope:{
            userid: '@',
            doctorid:'@',
            muted: '=',
            minWidth: '=',
            broadcastHandler: '&handleBroadcast',
            maxNumPeers:'='
        },
        link: function(scope, el, attrs){
            scope.mirror = attrs.mirror === 'true';
            scope.muted = attrs.muted === 'true';
        },
        controller: function($scope, $rootScope){
            //Setting global variables
            var userId = $scope.userid;
            var doctorId = $scope.doctorid;
            var webrtc;

            formRTCOptions = function(){
                //Creating options object
                var webRTCOptions = {
                    // the id/element dom element that will hold "our" video
                    localVideoEl: 'localVideo',
                    // the id/element dom element that will hold remote videos
                    remoteVideosEl: 'remotesVideos',
                    // immediately ask for camera access
                    autoRequestMedia: true,
                    //debugging capabilities
                    debug: false,
                    //media properties
                    media:{
                        audio: true,
                        video: true
                    }
                };
                //If muted is true
                if ($scope.muted) {
                    console.log('Muting!');
                    webRTCOptions.media = {
                        audio: false,
                        video: true
                    };
                }
                //If width was specified
                if ($scope.minWidth) {
                    var minWidth = parseInt($scope.minWidth);
                    if (typeof webRTCOptions.media.video !== 'object') {
                        webRTCOptions.media.video = {};
                    }
                    webRTCOptions.media.video.mandatory = {
                        minWidth: minWidth,
                        maxWidth: minWidth
                    };
                }
                return webRTCOptions;
            };
            //When prepare signal is received we instantiate the object
            $scope.$on('prepare', function prepareToBroadcast() {
                if (webrtc) {
                    console.log('already has prepared');
                    return;
                }
                //Creating options object
                var webRTCOptions = formRTCOptions();
                //Instantiating webrtc object
                webrtc = new SimpleWebRTC(webRTCOptions);
                //Passing additional parameters to the already existent WebRTC object
                postCreateRTCOptions(webrtc);
                //Passing webrtc object to $rootScope
                $rootScope.webrtc = webrtc;
                //When ready to call
                webrtc.on('readyToCall', function () {
                    //Creating room name
                    $scope.room = userId + '-' + doctorId;
                    console.log('joining room = ', $scope.room);
                    //Joining room
                    webrtc.joinRoom($scope.room);
                    //Initializing event handlers
                    rtcEventHandler(webrtc);
                });
                //Leave room handler
                $scope.$on('leaveRoom', function leaveRoom() {
                    console.log('leaving $scope.room', $scope.room);
                    if (!$scope.room) {
                        return;
                    }
                    webrtc.leaveRoom($scope.room);
                });
            });

            $scope.prepareRoom = function(){
                console.log('Preparing room!');
                $scope.$broadcast('prepare');
            };

            $scope.leaveRoom = function(){
                console.log('Leaving room!');
                $scope.$broadcast('leaveRoom');
            };

            //WebRTC Event handlers
            function rtcEventHandler(webrtc){
                webrtc.on('videoRemoved', function (video, peer) {
                    console.log('videoRemoved!');
                });
                webrtc.on('videoAdded', function (video, peer) {
                    console.log('video added from peer nickname', peer.nick);
                });
                webrtc.on('iceFailed', function (peer) {
                    console.error('ice failed', peer);
                });
                webrtc.on('connectivityError', function (peer) {
                    console.error('connectivity error', peer);
                });
                webrtc.on('joinedRoom', function (name) {
                    console.log('joined room "%s"', name);
                    var peers = webrtc.getPeers();
                    if (peers && Array.isArray(peers) &&
                        peers.length > $scope.maxNumPeers) {
                        console.error('Too many people in the room, leaving');
                        webrtc.leaveRoom();
                        return;
                    }
                });
                //Chat messaging handler
                $scope.$on('messageAll', function (event, message) {
                    if (message && webrtc) {
                        webrtc.sendDirectlyToAll(JSON.stringify(message));
                    }
                });
            }
            
            function postCreateRTCOptions(webrtc){
                webrtc.config.localVideo.mirror == Boolean($scope.mirror);
                if($scope.muted){
                    webrtc.mute();
                }
            }

            /*TODO
             *
                *
                 *  Add method to handle
                 *  rejoining a room!
                 *
                 *
               *
             *
             */
        }
    }
});
