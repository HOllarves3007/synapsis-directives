'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */
describe('Synapsis App', function() {
  it('should automatically redirect to / when location hash/fragment is empty', function() {
    browser.get('some_random_url.html');
    expect(browser.getLocationAbsUrl()).toMatch("/");
  });

  describe('videoChat', function() {
    beforeEach(function() {
      browser.get('index.html#!/videoChat');
    });
  });

  describe('chat', function() {
    beforeEach(function() {
      browser.get('index.html#!/chat');
    });
  });
});
